const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('node_modules/popper.js/dist/popper.js', 'public/js').sourceMaps()
    .js('resources/js/sidebar.js', 'public/js')
    .js('resources/js/slickCarousel.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/general.scss', 'public/css')
    .sass('resources/sass/launches/completed_launches.scss', 'public/css')
    .sass('resources/sass/launches/favorites.scss', 'public/css')
    .sass('resources/sass/launches/launch.scss', 'public/css')
    .sass('resources/sass/launches/next_launches.scss', 'public/css')
    .sass('resources/sass/auth/login.scss', 'public/css')
    .sass('resources/sass/auth/register.scss', 'public/css')
    .sass('resources/sass/home.scss', 'public/css')
    .sass('resources/sass/admin/all_users.scss', 'public/css')
;
