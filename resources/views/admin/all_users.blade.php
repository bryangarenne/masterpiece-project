@extends('layouts.app')

@push('styles')
    <link href="{{ asset('css/all_users.css') }}" rel="stylesheet">
@endpush

@section('content')
    <section class="users-list">
        <h2>Utilisateurs</h2>
        @foreach ($users as $user)
            <div class="user-container">
                <div class="user-circle">
                    <p>{{ substr($user->name, 0, 1) }}</p>
                </div>
                <div class="user-data">
                    <p>Email : {{ $user->email }}</p>
                    <p>Prénom : {{ $user->name }}</p>
                </div>
                <delete-user-button id="{{ $user->id }}"></delete-user-button>
            </div>
        @endforeach
    </section>

    <section class="users-list">
        <h2>Administrateurs</h2>
        @foreach ($admins as $admin)
            <div class="user-container">
                <div class="user-circle">
                    <p>{{ substr($admin->name, 0, 1) }}</p>
                </div>
                <div class="user-data">
                    <p>Email : {{ $admin->email }}</p>
                    <p>Prénom : {{ $admin->name }}</p>
                </div>
            </div>
        @endforeach
    </section>
@endsection
