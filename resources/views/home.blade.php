@extends('layouts.app')

@push('styles')
    <link href="{{ asset('css/home.css') }}" rel="stylesheet">
@endpush

@section('content')

@include('launches/next_launch')

@auth
    <section class="favorites-launches-section">
        <a class="link-page" href="/favorites"><h2 class="favorites-launches-title">Mes favoris</h2></a>
        <div class="carousel favorites-launches-carousel">
            @foreach($all_favorites_launches as $launch)
                <div class="item">
                    <a href="{{"/launch/" .  $launch["id"] }}" class="launch-link">
                        @if($launch["rocket"]["imageURL"] === "Array")
                            <img src="{{ asset('storage/images/img_launch_default.png') }}" alt="">
                        @else
                            <img src="{{ $launch["rocket"]["imageURL"] }}" alt="">
                        @endif
                        <p class="item-launch-name">{{ $launch["name"] }}</p>
                    </a>
                </div>
            @endforeach
        </div>
    </section>
@endauth

<section class="next-launches-section">
    <a class="link-page" href="/soon"><h2 class="soon-launches-title">Les prochains lancements</h2></a>
    <div class="carousel next-launches-carousel">
        @foreach($next_launches as $launch)
            <div class="item">
                <a href="{{"/launch/" .  $launch["id"] }}" class="launch-link">
                    @if($launch["rocket"]["imageURL"] === "Array")
                        <img src="{{ asset('storage/images/img_launch_default.png') }}" alt="">
                    @else
                        <img src="{{ $launch["rocket"]["imageURL"] }}" alt="">
                    @endif
                    <p class="item-launch-name">{{ $launch["name"] }}</p>
                </a>
            </div>
        @endforeach
    </div>
</section>

<section class="completed-launches-section">
    <a class="link-page" href="/completed"><h2 class="completed-launches-title">Lancements terminés</h2></a>
    <div class="carousel completed-launches-carousel">
        @foreach($completed_launches as $launch)
            <div class="item">
                <a href="{{"/launch/" .  $launch["id"] }}" class="launch-link">
                    @if($launch["rocket"]["imageURL"] === "Array")
                        <img src="{{ asset('storage/images/img_launch_default.png') }}" alt="">
                    @else
                        <img src="{{ $launch["rocket"]["imageURL"] }}" alt="">
                    @endif
                    <p class="item-launch-name">{{ $launch["name"] }}</p>
                </a>
            </div>
        @endforeach
    </div>
</section>
@endsection



