<nav class="navbar navbar-expand-lg">
    <a class="navbar-brand" href="/">
        <img src="{{ asset('storage/images/Logos_Laïka_Program/Laika_Blanc_Transparant.png') }}" width="50" height="50" alt="Laïka Program logo">
    </a>

    <ul class="nav mr-auto">
        <li class="nav-item">
            <a  class="nav-link" href="/soon">Prochainement</a>
        </li>
    </ul>

    <ul class="nav ml-auto">
        @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
        @endguest
    </ul>

    <button class="toggler" id="menu-toggle" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <img src="{{ asset('storage/images/menu-white.svg') }}"alt="Menu icon">
    </button>
</nav>
