<div id="sidebar-wrapper">
    <a href="javascript:void(0)" id="close-sidebar">
        <img src="{{ asset('storage/images/close.svg') }}" alt="Menu icon">
    </a>
    <div class="sidebar-heading">Laïka Program</div>
    <div class="list-group list-group-flush">
        <a href="/soon" class="list-group-item list-group-item-action">Prochainement</a>
        <a href="/completed" class="list-group-item list-group-item-action">Terminés</a>

        @auth
            <a href="/favorites" class="list-group-item list-group-item-action">Mes favoris</a>

            @if(Auth::user()->role === 'ADMIN')
                <a href="/admin/users" class="list-group-item list-group-item-action">Utilisateurs</a>
            @endif

            <a class="list-group-item list-group-item-action logout-link" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
{{--                {{ __('Logout') }}--}}
                Déconnexion
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        @else
            @if (Route::has('register'))
                <a class="list-group-item list-group-item-action" href="{{ route('register') }}">
{{--                    {{ __('Register') }}--}}
                    Inscription
                </a>
            @endif
        @endauth
    </div>
</div>
