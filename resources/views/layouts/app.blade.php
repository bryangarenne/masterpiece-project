<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Laïka Program</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        @stack('styles')
        <link href="{{ asset('css/general.css') }}" rel="stylesheet">
        <link rel="shortcut icon" type="image/png" href="{{ asset('storage/images/Logos_Laïka_Program/Laika_Blanc_Transparant.png') }}"/>
    </head>
    <body>
    <div class="d-flex toggled" id="app">

        <div id="page-content-wrapper">
            <header>
                @include('layouts.navbar')

                @isset($page_launch)
                    <div class="map-container">
                        <launch-pad-map latitude="{{ $launch["location"]["pads"][0]["latitude"] }}" longitude="{{ $launch["location"]["pads"][0]["longitude"] }}"></launch-pad-map>
                        <div class="location-name-container">
                            <div class="location-name-content">
                                <p>{{ $launch["location"]["pads"][0]["name"] }}</p>
                            </div>
                        </div>
                    </div>
                @endisset

                @isset($page_home)
                    <div class="background-header-home">
                        <img src="{{ asset('storage/images/header_background.jpg') }}" alt="">
                    </div>
                @endisset
            </header>

            <div class="container-fluid">
                @yield('content')
            </div>
        </div>

        @include('layouts.sidebar')
    </div>


    <!-- App scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Menu Toggle Script -->
    <script src="{{ asset('js/sidebar.js') }}" defer></script>
    <!-- Slick -->
    <script src="{{ asset('js/slickCarousel.js') }}" defer></script>
    </body>
</html>
