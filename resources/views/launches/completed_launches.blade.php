@extends('layouts.app')

@push('styles')
    <link href="{{ asset('css/completed_launches.css') }}" rel="stylesheet">
@endpush

@section('content')
    <h1 class="title-page">Lancements terminés</h1>

    <section class="completed-launches-section launches-section">
        <div class="completed-launches-container launches-container">
            @foreach($completed_launches as $launch)
                @include('launches.launch_card')
            @endforeach
        </div>
    </section>
@endsection
