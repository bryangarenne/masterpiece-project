@extends('layouts.app')

@push('styles')
    <link href="{{ asset('css/launch.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="alert-container"></div>
    <h1 class="title-page">{{ $launch["name"] }}</h1>

    <section class="launch-details-section">
        <div class="launch-container">
            <div class="img-date-container">
                <div class="launch-img-container">
                    @if($launch["rocket"]["imageURL"] === "Array")
                        <img src="{{ asset('storage/images/img_launch_default.png') }}" alt="launch rocket">
                    @else
                        <img src="{{ $launch["rocket"]["imageURL"] }}" alt="launch rocket">
                    @endif
                        @if(getdate(strtotime( $launch["net"]))[0] > $timestamp)
                        <countdown-timer date="{{$launch["net"]}}"></countdown-timer>
                        @endif
                </div>
                <div class="date-container">
                    <p class="launch-info net-date">
                        <span>Date de lancement : </span><br>{{ $launch["net"] }}
                    </p>
                    <p class="launch-info window-start-date">
                        <span>Window start : </span><br>{{ $launch["windowstart"] }}
                    </p>
                    <p class="launch-info window-end-date">
                        <span>Window end : </span><br>{{ $launch["windowend"] }}
                    </p>
                </div>
            </div>

            <div class="launch-infos">
                <p class="launch-info spatial-agency">
                    @empty(!$launch["rocket"]["agencies"])
                    <span>Agence spatial : </span>{{ $launch["rocket"]["agencies"][0]["name"] }}
                    @endempty

                </p>
                <p class="launch-info rocket-name">
                    <span>Lanceur : </span>{{ $launch["rocket"]["name"] }}
                </p>
                @empty(!$launch["missions"])
                <p class="launch-info mission-name">
                    <span>Mission : </span>{{ $launch["missions"][0]["name"] }}
                </p>
                <p class="launch-info description">
                    <span>Description</span><br>{{ $launch["missions"][0]["description"] }}
                </p>
                @endempty
            </div>
        </div>

        <div class="button-container">
            @if($launch["vidURLs"])
                <a href="{{ $launch["vidURLs"][0] }}" class="play-icon-container">
                    <img src="{{ asset('storage/images/play.svg') }}" class="icon-container" alt="play">
                </a>
            @endif

            @auth
                @if($favorite_exist)
                    <star-favorite-button v-bind:is-exist="true" launch-id="{{ $id }}"></star-favorite-button>
                @else
                    <star-favorite-button v-bind:is-exist="false" launch-id="{{ $id }}"></star-favorite-button>
                @endif
            @endauth
        </div>
    </section>
@endsection
