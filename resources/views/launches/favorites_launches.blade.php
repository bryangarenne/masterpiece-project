@extends('layouts.app')

@push('styles')
    <link href="{{ asset('css/favorites.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="alert-container"></div>
    <h1 class="title-page">Mes favoris</h1>

    <section class="launches-section">
        <h2>A venir</h2>
        <div class="launches-container">
            @foreach($all_favorites_launches as $launch)
                @if($timestamp <= strtotime($launch["net"]))
                    @include('launches.launch_card')
                @endif
            @endforeach
        </div>
    </section>

    <section class="launches-section">
        <h2>Terminés</h2>
        <div class="launches-container">
            @foreach($all_favorites_launches as $launch)
                @if($timestamp > strtotime($launch["net"]))
                    @include('launches.launch_card')
                @endif
            @endforeach
        </div>
    </section>
@endsection

