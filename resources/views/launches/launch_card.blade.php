<div class="launch-container">


    <div class="launch-img-container">
        <a href="{{"/launch/" . $launch["id"] }}" class="launch-link">
            @if($launch["rocket"]["imageURL"] === "Array")
                <img src="{{ asset('storage/images/img_launch_default.png') }}" alt="">
            @else
                <img src="{{ $launch["rocket"]["imageURL"] }}" alt="">
            @endif
        </a>
    </div>


    <div class="launch-content">
        <h3 class="launch-name">{{ $launch["name"] }}</h3>
        <p class="net-date">{{ $launch["net"] }}</p>
        @empty(!$launch["missions"])
            <p class="description">{{ substr($launch["missions"][0]["description"], 0, 250) }}</p>
        @endempty
            @isset($all_favorites_launches)
                <delete-favorite-button id="{{ $launch["id"] }}"></delete-favorite-button>
            @endif
    </div>
</div>
