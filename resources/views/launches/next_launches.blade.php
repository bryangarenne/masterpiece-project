@extends('layouts.app')

@push('styles')
    <link href="{{ asset('css/next_launches.css') }}" rel="stylesheet">
@endpush

@section('content')
    <h1 class="title-page">Lancements à venir</h1>

    <section class="launches-section">
        @include('launches.next_launch')
        <h2>Lancements</h2>
        <div class="launches-container">
            @foreach($next_launches as $launch)
                @include('launches.launch_card')
            @endforeach
        </div>
    </section>
@endsection

