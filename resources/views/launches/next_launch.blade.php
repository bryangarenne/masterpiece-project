<section class="launch-section">
    <h2 class="launch-title">Le prochain lancement</h2>
    <div class="launch-container next-launch-container">
        <div class="image-and-chrono">
            <div class="launch-img-container">
                <a href="{{"/launch/" . $next_launches[0]["id"] }}" class="launch-link">
                    @if($next_launches[0]["rocket"]["imageURL"] === "Array")
                        <img src="{{ asset('storage/images/img_launch_default.png') }}" alt="">
                    @else
                        <img src="{{ $next_launches[0]["rocket"]["imageURL"] }}" alt="">
                    @endif
                </a>
            </div>

            <countdown-timer date="{{$next_launches[0]["net"]}}"></countdown-timer>
        </div>

        <div class="launch-content">
            <h3 class="launch-name">{{ $next_launches[0]["name"] }}</h3>
            <p class="net-date">{{ $next_launches[0]["net"] }}</p>
            @empty(!$next_launches[0]["missions"])
                <p class="description">{{ substr($next_launches[0]["missions"][0]["description"], 0, 400) }}</p>
            @endempty
        </div>
    </div>
</section>
