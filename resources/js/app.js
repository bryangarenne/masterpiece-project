/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import $ from 'jquery';
import 'jquery-ui/ui/widgets/datepicker.js';
import 'slick-carousel/slick/slick.min';
import 'leaflet/dist/leaflet';

require('./bootstrap');
require('@fortawesome/fontawesome-free/js/all.js');

window.Vue = require('vue');
window.$ = window.jQuery = $;

// $('#datepicker').datepicker();
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component('delete-favorite-button', require('./components/delete-favorite-button').default);
Vue.component('star-favorite-button', require('./components/star-favorite-button').default);
Vue.component('delete-user-button', require('./components/delete-user-button').default);
Vue.component('launch-pad-map', require('./components/launch-pad-map').default);
Vue.component('countdown-timer', require('./components/countdown-timer').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

if(document.querySelector('.leaflet-control-zoom') !== null) {
    document.querySelector('.leaflet-control-zoom').style.display= "none";
    document.querySelector('.leaflet-bottom.leaflet-right').style.display= "none";
}
