<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/soon', 'LaunchController@soon');
Route::get('/completed', 'LaunchController@completed');
Route::get('/favorites', 'FavoriteController@index');
Route::get('/launch/{id}', 'LaunchController@showLaunch');
Route::get('/admin/users', 'AdminController@index')->name('users')->middleware('isAdmin');

Route::post('/favorite/add', 'AjaxController@addLaunchToFavorites');
Route::post('/favorite/delete', 'AjaxController@deleteLaunchToFavorites');


Route::post('/user/delete', 'AdminController@deleteUser');
