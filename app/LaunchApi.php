<?php


namespace App;


class LaunchApi
{
    public function getApiData($url) {
        $curl = curl_init($url);
        $certificate = 'cacert.pem';

        curl_setopt($curl, CURLOPT_CAINFO, __DIR__ . DIRECTORY_SEPARATOR . $certificate);
        curl_setopt($curl, CURLOPT_CAPATH, __DIR__ . DIRECTORY_SEPARATOR . $certificate);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $data = curl_exec($curl);

        if  ($data === false) {
            var_dump(curl_error($curl));
        }

        curl_close($curl);

        return json_decode($data, true);
    }
}
