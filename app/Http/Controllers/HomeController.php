<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $next_launches = App::make('nextLaunchesService')->get_next_launches();
        $completed_launches = App::make('completedLaunchesService')->get_completed_launches();

        if(Auth::check()){
            $all_favorites_launches = App::make('favoritesService')->get_all_favorites();
            return view('home', [ 'page_home' => "home", 'next_launches' => $next_launches, 'completed_launches' => $completed_launches, 'all_favorites_launches' => $all_favorites_launches]);
        } else {
            return view('home', [ 'page_home' => "home", 'next_launches' => $next_launches, 'completed_launches' => $completed_launches]);
        }
    }
}
