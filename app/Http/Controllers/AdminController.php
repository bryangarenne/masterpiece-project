<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }

    public function index()
    {
        $users = DB::table('users')
            ->where('role', '=', null)
            ->get();

        $admins = DB::table('users')
            ->where('role', '=', 'ADMIN')
            ->get();
        return view('admin.all_users', ['users' => $users, 'admins' => $admins]);
    }

    public function deleteUser(Request $request) {
        $id = $request->get('id');
        DB::table('users')
            ->where('id', '=', $id)
            ->delete()
        ;
    }
}
