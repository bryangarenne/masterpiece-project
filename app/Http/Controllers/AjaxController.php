<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AjaxController extends Controller
{
    /**
     * add Launch To Favorites
     * @param Request $request
     */
    public function addLaunchToFavorites(Request $request) {
        $launch_id = $request->get('id');
        $user = auth()->user();
        DB::table('favorites')->insert([
            'user_id' => $user->id,
            'launch_id' => $launch_id
        ]);
    }

    /**
     * delete Launch To Favorites
     * @param Request $request
     */
    public function deleteLaunchToFavorites(Request $request) {
        $launch_id = $request->get('id');
        Log::info($launch_id);
        DB::table('favorites')
            ->where('launch_id', '=', $launch_id)
            ->delete()
        ;
    }
}
