<?php

namespace App\Http\Controllers;

use App\LaunchApi;
use DateTime;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class LaunchController extends Controller
{
    /**
     * Retourne la vue des prochains lancements
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function soon()
    {
        $next_launches = App::make('nextLaunchesService')->get_next_launches();
        return view('launches.next_launches', ['next_launches' => $next_launches]);
    }

    /**
     * Retourne la vue des lancements terminés
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function completed()
    {
        $completed_launches = App::make('completedLaunchesService')->get_completed_launches();
        return view('launches.completed_launches', ['completed_launches' => $completed_launches]);
    }

    /**
     * Retourne la vue d'un lancement
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLaunch($id)
    {
        $url = 'https://launchlibrary.net/1.4/launch/' . $id;
        $user = auth()->user();
        $ApiData = new LaunchApi();
        $result = $ApiData->getApiData($url);
        $date = new DateTime();
        $timestamp =  $date->getTimestamp();

        if(isset($user)) {
            $favorite_exist = DB::table('favorites')
                ->where('launch_id', $id)
                ->where('user_id', '=', $user->id)
                ->exists()
            ;
        } else {
            $favorite_exist = false;
        }




        if (empty($result["launches"]) || $result["launches"][0]["id"] != $id) {
            return redirect(route('home'));
        } else {
            $launch = $result["launches"][0];
            return view( 'launches.launch', ['page_launch' => "launch",'id' => $id, 'launch' => $launch, 'favorite_exist' => $favorite_exist, 'timestamp' => $timestamp]);
        }
    }
}
