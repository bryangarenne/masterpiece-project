<?php

namespace App\Http\Controllers;

use DateTime;
use Exception;
use Illuminate\Support\Facades\App;

class FavoriteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws Exception
     */
    public function index()
    {
        $date = new DateTime();
        $timestamp =  $date->getTimestamp();

        $all_favorites_launches = App::make('favoritesService')->get_all_favorites();
        return view('launches.favorites_launches', ['all_favorites_launches' => $all_favorites_launches, 'timestamp' => $timestamp]);
    }
}
