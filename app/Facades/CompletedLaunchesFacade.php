<?php


namespace App\Facades;


class CompletedLaunchesFacade
{
    protected static function get_completed_launches()
    {
        return 'completedLaunchesService';
    }
}
