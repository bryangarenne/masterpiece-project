<?php


namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class NextLaunchesFacade extends Facade
{
    protected static function get_next_launches()
    {
        return 'nextLaunchesService';
    }
}
