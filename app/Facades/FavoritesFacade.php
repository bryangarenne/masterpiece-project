<?php


namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class FavoritesFacade extends Facade
{
    protected static function get_all_favorites()
    {
        return 'favoritesService';
    }
}
