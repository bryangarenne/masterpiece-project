<?php

namespace App\Providers;

use App\FavoritesService;
use Illuminate\Support\ServiceProvider;

class FavoritesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('favoritesService', function($app){
            return new FavoritesService;
        });
    }
}
