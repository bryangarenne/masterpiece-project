<?php

namespace App\Providers;

use App\NextLaunchesService;
use Illuminate\Support\ServiceProvider;

class NextLaunchesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('nextLaunchesService', function($app){
            return new NextLaunchesService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
