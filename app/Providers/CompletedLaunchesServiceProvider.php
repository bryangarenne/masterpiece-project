<?php

namespace App\Providers;

use App\CompletedLaunchesService;
use App\FavoritesService;
use Illuminate\Support\ServiceProvider;

class CompletedLaunchesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('completedLaunchesService', function($app){
            return new CompletedLaunchesService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
