<?php

namespace App;

use App\LaunchApi;
use Illuminate\Support\Facades\DB;

class FavoritesService
{
    public function get_all_favorites()
    {
        $all_favorites_launches = [];
        $user = auth()->user();
        $ApiData = new LaunchApi();

        $all_favorites = $users = DB::table('favorites')
            ->having('user_id', '=', $user->id)
            ->get();

        $all_favorites = json_decode($all_favorites, true);

        foreach ($all_favorites as $favorite) {
            $url = 'https://launchlibrary.net/1.4/launch/' . $favorite["launch_id"];
            array_push($all_favorites_launches, $ApiData->getApiData($url)["launches"][0]);
        }
        return $all_favorites_launches;
    }
}
