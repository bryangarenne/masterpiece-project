<?php


namespace App;

use App\LaunchApi;
class NextLaunchesService
{
    public function get_next_launches() {
        $url = 'https://launchlibrary.net/1.4/launch/next/50';
        $ApiData = new LaunchApi();
        return $ApiData->getApiData($url)["launches"];
    }
}
