<?php


namespace App;


class CompletedLaunchesService
{
    function get_completed_launches() {
        $url = 'https://launchlibrary.net/1.4/launch/' . date('Y-m-d', strtotime('-1' .' year')) . '/' . date("Y-m-d") . '?limit=200';
        $ApiData = new LaunchApi();

        return array_reverse($ApiData->getApiData($url)["launches"]);
    }
}
