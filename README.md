# Laïka Program

### Never miss a rocket launch !

Author : Bryan Garenne

----------
# Server Requirements

* PHP >= 7.2.5
* Composer
* Node.js

# Getting started

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/7.x)

Clone the repository

    git clone git@gitlab.com:bryan_garenne/masterpiece-project.git
Switch to the repo folder

    cd masterpiece-project

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Create a database with the same name as the "DB_DATABASE" variable in the .env file
   
    DB_DATABASE=database_name-example

Install all the dependencies using composer and npm

    composer install
    npm install

Generate a new application key

    php artisan key:generate
    
Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Ajouter les medias dans la partie public

    php artisan storage:link
    
Ajouter les assets(JS, CSS) dans la partie public

    npm run dev

Start the local development server

    php artisan serve
    
You can now access the server at http://localhost:8000

**TL;DR command list**

    git clone git@gitlab.com:bryan_garenne/masterpiece-project.git
    cd masterpiece-project
    cp .env.example .env
    composer install
    npm install
    php artisan key:generate
    
**Make sure you set the correct database connection information before running the migrations** [Environment variables](#environment-variables)

    php artisan migrate
    php artisan serve
